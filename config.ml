open Base

type user =
  {
    pretty_name: string;
    gitlab_username: string;
  }

type t =
  {
    do_not_update: bool;
    state_filename: string;
    sleep_delay_in_minutes: int;
    users: user list;
    projects: string list;
    token: string option;
    token_filename: string option
  }

type arg =
  | User of user
  | Project of string

let user_rex = rex "^(.*)@(.+)$"
let project_rex = rex "^.+/.+$"

let help () =
  echo "Run with --help for more information.";
  exit 1

let parse_user_or_project s =
  match s =~** user_rex with
    | Some (pretty_name, gitlab_username) ->
        let pretty_name = if pretty_name = "" then gitlab_username else pretty_name in
        User { pretty_name; gitlab_username }
    | None ->
        if s =~ project_rex then
          Project s
        else (
          echo "Invalid user or project: %S" s;
          help ();
        )

let parse_cli () =
  let do_not_update =
    Clap.flag
      ~set_long: "do-not-update"
      ~set_short: 'd'
      false
  in
  let state_filename =
    Clap.default_string
      ~long: "state"
      ~placeholder: "FILE"
      ~description: "Path to the file where to store the state."
      "team-md.state"
  in
  let sleep_delay_in_minutes =
    Clap.default_int
      ~long: "delay"
      ~placeholder: "MINUTES"
      ~description: "How long, in minutes, to wait between two iterations."
      20
  in
  let token =
    Clap.optional_string
      ~long: "token"
      ~placeholder: "TOKEN"
      ~description: "GitLab authentication token. \
                     It can be obtain by visting https://gitlab.com/-/profile/personal_access_tokens.\n \
                     Only Read-Only access to the API is required.\n \
                     This option conflicts with --token-file.\n\
                     This option forces the HTTP server to bind to 127.0.0.1 only."
    ()
  in
  let token_filename =
    Clap.optional_string
      ~long: "token-file"
      ~placeholder: "PATH"
      ~description: "Path to a file containing only one line with a GitLab authentication token.\n \
                     This option conflicts with --token.\n\
                     This option forces the HTTP server to bind to 127.0.0.1 only."
    ()
  in
  let users_and_projects =
    Clap.list_string
      ~placeholder: "USER_OR_PROJECT"
      ~description:
        "Users and projects to track. \
         Users must be of the form: @username or pretty@username (e.g. Romain@romain.nl). \
         Projects must be of the form: group/project (e.g. tezos/tezos)."
      ()
  in
  Clap.close ();
  let users, projects =
    List.partition_map
      (function User x -> Left x | Project x -> Right x)
      (List.map parse_user_or_project users_and_projects)
  in
  if users = [] then (
    echo "You didn't specify any user to track.";
    help ();
  );
  if projects = [] then (
    echo "You didn't specify any project to track.";
    help ();
  );
  {
    do_not_update;
    state_filename;
    sleep_delay_in_minutes;
    users;
    projects;
    token;
    token_filename;
  }
