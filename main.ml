(* I have a custom tool that generates dune files from this and from the
   directory and file structure. Don't pay attention. *)
(* USES lwt *)
(* TLS is an optional dependency of conduit-lwt-unix, which is an optional
   dependency of cohttp-lwt-unix, and is needed for HTTPS. *)
(* USES tls *)
(* USES cohttp-lwt-unix *)
(* USES ezjsonm *)
(* USES re *)
(* USES clap *)

(* I copied the JSON and Base module from Tezt without remorse. *)
open Base

let main (config: Config.t) =
  let users_to_track =
    String_set.of_list
      (List.map (fun (user: Config.user) -> user.gitlab_username) config.users)
  in
  let name_map =
    List.fold_left
      (fun acc (user: Config.user) ->
         String_map.add user.gitlab_username user.pretty_name acc)
      String_map.empty config.users
  in

  (* Read state. *)
  let state, first_time =
    if Sys.file_exists config.state_filename then (
      let state = State.load config.state_filename in
      echo "Loaded state from: %s" config.state_filename;
      state, false
    )
    else (
      echo "%s does not exist, will start from an empty state." config.state_filename;
      State.empty, true
    )
  in

  let* token =
    match config.token, config.token_filename with
      | Some _, Some _ ->
          echo "Cannot specify both --token and --token-file.";
          exit 1
      | x, None ->
          Lwt.return x
      | None, Some path ->
          if not (Sys.file_exists path) then (
            echo "Token file %s does not exist. " path;
            exit 1
          );
          let* token = read_file path in
          echo "Loaded token from: %s" path;
          Lwt.return_some token
  in

  (* Done initializing. *)
  echo "Ready.";

  (* Main loop that updates the state. *)
  let state = ref state in
  let update_once first_time =
    let day: State.Day.t =
      let tm = Unix.(gmtime (time ())) in
      {
        year = tm.tm_year + 1900;
        month = tm.tm_mon + 1;
        day = tm.tm_mday;
      }
    in
    let* topics =
      if config.do_not_update then
        return []
      else
        Gitlab.get_topics
          ~token: config.token
          ~projects: config.projects
          ~users: (List.map (fun (user: Config.user) -> user.gitlab_username) config.users)
    in
    state := State.update_with_list users_to_track day !state topics;
    (* We want to check if topics have been closed.
       [Gitlab.get_topics] only returns opened topics. *)
    let up_to_date =
      List.map (fun (topic: State.Topic.t) -> topic.id) topics |> State.ID_set.of_list
    in
    let checked_if_closed = ref 0 in
    let check_if_closed (_, (topic: State.Topic.t)) =
      if State.ID_set.mem topic.id up_to_date then
        unit
      else
      if
        not (String_set.mem topic.author users_to_track) &&
        String_set.is_empty (String_set.inter topic.assignees users_to_track)
      then
        unit
      else
        let* topic = Gitlab.get_topic ~token topic.id in
        state := State.update users_to_track day !state topic;
        incr checked_if_closed;
        unit
    in
    let* () = Lwt_list.iter_p check_if_closed (State.ID_map.bindings (!state).open_topics) in
    (* Don't fill the timeline the first time since it will just list everything. *)
    if first_time then state := State.clear_timeline !state;
    State.save config.state_filename !state;
    echo "Updated state with %d + %d topics." (List.length topics) !checked_if_closed;
    unit
  in
  let handle_error exn =
    echo "Error: %s" (Printexc.to_string exn);
    unit
  in
  let rec main_update_loop first_time =
    let* () =
      Lwt.catch
        (fun () -> update_once first_time)
        handle_error
    in
    let* () = Lwt_unix.sleep (float (config.sleep_delay_in_minutes * 60)) in
    main_update_loop false
  in
  let* () = main_update_loop first_time
  and* () =
     let* ctx =
       match token with
          | Some _ ->
             let* ctx = Conduit_lwt_unix.init ~src:"127.0.0.1" () in
             Lwt.return @@ Some (Cohttp_lwt_unix.Client.custom_ctx ~ctx () )
          | None -> Lwt.return_none
     in
    Cohttp_lwt_unix.Server.(
      create ?ctx ~mode: (`TCP (`Port 8000))
        (make ~callback: (Server.serve (fun () -> !state) name_map) ())
    )
  in
  unit

let rec main_loop config =
  (
    try
      Lwt_main.run (main config)
    with exn ->
      echo "Main loop crashed: %s" (Printexc.to_string exn);
      echo "Restarting...";
  );
  Unix.sleep (config.sleep_delay_in_minutes * 60);
  main_loop config

let () =
  let config = Config.parse_cli () in
  echo "Projects to track:";
  List.iter (echo "- %s") config.projects;
  echo "Users to track:";
  let echo_user (user: Config.user) =
    echo "- %s (GitLab username: %s)" user.pretty_name user.gitlab_username
  in
  List.iter echo_user config.users;
  main_loop config
