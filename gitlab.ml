open Base

let get ~token ~project url =
  let url =
    Uri.of_string (
      "https://gitlab.com/api/v4/projects/" ^ Uri.pct_encode project ^ "/" ^ url
    )
  in
  let headers = Option.map (Cohttp.Header.(add (init ()) "PRIVATE-TOKEN")) token in
  let* (response, response_body) = Cohttp_lwt_unix.Client.call
  ?headers `GET url in
  let* response_body = Cohttp_lwt.Body.to_string response_body in
  match response.status with
    | #Cohttp.Code.success_status ->
        return (JSON.parse ~origin: "gitlab_response" response_body)
    | status ->
        failwith @@ sf "Slack responded with %s - %s"
          (Cohttp.Code.string_of_status status)
          response_body

type kind = Issue | MR

let as_user json =
  JSON.(json |-> "username" |> as_string)

let as_status json: State.Topic.status =
  match JSON.as_string json with
    | "opened" -> Open
    | "closed" -> Closed
    | "merged" -> Merged
    | s -> failwith @@ sf "Unknown status: %s" s

let as_topic kind ~project json: State.Topic.t =
  let iid = JSON.(json |-> "iid" |> as_int) in
  let id: State.ID.t =
    match kind with
      | Issue -> Issue (project, iid)
      | MR -> MR (project, iid)
  in
  {
    id;
    title = JSON.(json |-> "title" |> as_string);
    milestone = JSON.(json |-> "milestone" |-> "title" |> as_string_opt);
    author = JSON.(json |-> "author" |> as_user);
    assignees =
      JSON.(json |-> "assignees" |> as_list |> List.map as_user |> String_set.of_list);
    status = JSON.(json |-> "state" |> as_status);
    approvals = String_set.empty; (* filled in later *)
  }

let get_approvals ~token ~project ~merge_request_id =
  let* json = get ~token ~project (sf "merge_requests/%d/approvals" merge_request_id) in
  JSON.(
    json |-> "approved_by" |> as_list |> List.map @@ fun json ->
    json |-> "user" |-> "username" |> as_string
  )
  |> String_set.of_list |> return

let get_merge_requests ~token ~project ~by ~user =
  let* json = get ~token ~project (sf "merge_requests?%s=%s&state=opened" by user) in
  let mrs = JSON.as_list json |> List.map (as_topic MR ~project) in
  mrs |> Lwt_list.map_p @@ fun (mr: State.Topic.t) ->
  match mr.id with
    | Issue _ ->
        return mr
    | MR (project, id) ->
        let* approvals = get_approvals  ~token ~project ~merge_request_id: id in
        return { mr with approvals }

let get_issues ~token ~project ~by ~user =
  let* json = get ~token ~project (sf "issues?%s=%s&state=opened" by user) in
  return (JSON.as_list json |> List.map (as_topic Issue ~project))

let get_topic (id: State.ID.t) ~token =
  match id with
    | Issue (project, id) ->
        let* json = get ~token ~project (sf "issues/%d" id) in
        return (as_topic Issue ~project json)
    | MR (project, id) ->
        let* json = get ~token ~project (sf "merge_requests/%d" id) in
        return (as_topic MR ~project json)

let get_topics ~token ~projects ~users =
  let get_for_user user =
    let* mrs1 =
      Lwt_list.map_p
        (fun project -> get_merge_requests ~token ~project ~by: "author_username" ~user)
        projects
    in
    let* mrs2 =
      Lwt_list.map_p
        (fun project -> get_merge_requests ~token ~project ~by: "assignee_username" ~user)
        projects
    in
    let* issues1 =
      Lwt_list.map_p
        (fun project -> get_issues ~token ~project ~by: "author_username" ~user)
        projects
    in
    let* issues2 =
      Lwt_list.map_p
        (fun project -> get_issues ~token ~project ~by: "assignee_username" ~user)
        projects
    in
    return @@ List.flatten @@ List.flatten [ mrs1; mrs2; issues1; issues2 ]
  in
  let* topics = Lwt_list.map_p get_for_user users in
  return (List.flatten topics)
